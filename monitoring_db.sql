-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~xenial.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 17, 2018 at 09:16 AM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 5.6.37-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `monitoring_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `akibat_kerja`
--

CREATE TABLE `akibat_kerja` (
  `id` int(11) NOT NULL,
  `id_data_sementara` int(11) DEFAULT NULL,
  `berita_acara` text,
  `laporan_penyakit` text,
  `surat_keterangan` text,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cacat`
--

CREATE TABLE `cacat` (
  `id` int(11) NOT NULL,
  `id_data_sementara` int(11) DEFAULT NULL,
  `berita_acara` text,
  `laporan_kronologis` text,
  `surat_keterangan` text,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_sementara`
--

CREATE TABLE `data_sementara` (
  `id` int(11) NOT NULL,
  `no` varchar(50) DEFAULT NULL,
  `tanggal_laporan` date DEFAULT NULL,
  `nama_pelapor` varchar(50) DEFAULT NULL,
  `nama_ymk` varchar(50) DEFAULT NULL,
  `NIP` varchar(50) DEFAULT NULL,
  `tempat_kejadian` varchar(50) DEFAULT NULL,
  `waktu_kejadian` datetime DEFAULT NULL,
  `rs_faskes` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_sementara`
--

INSERT INTO `data_sementara` (`id`, `no`, `tanggal_laporan`, `nama_pelapor`, `nama_ymk`, `NIP`, `tempat_kejadian`, `waktu_kejadian`, `rs_faskes`) VALUES
(4, 'R', '2018-09-20', '12', 'qw1', '1231', 'so', '2019-12-12 00:00:00', 'os'),
(5, 'R', '2018-09-18', '12', 'qw1', '1231', 'so', '2019-12-12 00:00:00', 'os'),
(7, '1231', '2018-09-17', 'updateok', 'updateok', '', 'updateok', '1998-12-20 00:00:00', '12i'),
(8, 'tes', '2012-12-12', 'ok', 'ko', 'ok', 'ok', '2019-12-12 00:00:00', 'okas'),
(9, '2018-09-17', '2018-09-17', 'as', 'as', '', 'Rbg', '2018-09-19 15:30:00', '0121');

-- --------------------------------------------------------

--
-- Table structure for table `perawatan`
--

CREATE TABLE `perawatan` (
  `id` int(11) NOT NULL,
  `id_data_sementara` int(11) DEFAULT NULL,
  `berita_acara` text,
  `surat_jaminan_rs` text,
  `surat_jaminan_jr` text,
  `surat_bpjs` text,
  `surat_kepastian_jaminan` text,
  `surat_pernyataan_tidak_menjamin` text,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tewas`
--

CREATE TABLE `tewas` (
  `id` int(11) NOT NULL,
  `id_data_sementara` int(11) DEFAULT NULL,
  `berita_acara` text,
  `laporan_kronologis` text,
  `usulan_penetapan` text,
  `verifikasi_bkn` text,
  `penetapan_tewas` text,
  `surat_penegasan` text,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'root', 'ff9830c42660c1dd1942844f8069b74a');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akibat_kerja`
--
ALTER TABLE `akibat_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cacat`
--
ALTER TABLE `cacat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_sementara`
--
ALTER TABLE `data_sementara`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perawatan`
--
ALTER TABLE `perawatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tewas`
--
ALTER TABLE `tewas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akibat_kerja`
--
ALTER TABLE `akibat_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cacat`
--
ALTER TABLE `cacat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_sementara`
--
ALTER TABLE `data_sementara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `perawatan`
--
ALTER TABLE `perawatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tewas`
--
ALTER TABLE `tewas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
