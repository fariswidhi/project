<?php 

@$mulai = $_GET['mulai'];
@$sampai = $_GET['sampai'];
$sql = "SELECT * FROM perawatan a INNER JOIN data_sementara b on a.id_data_sementara = b.id ";
if (!empty($mulai)) {
  # code...
  $sql .= " WHERE b.tanggal_laporan >='".$mulai."' AND b.tanggal_laporan <= '".$sampai."'";
}
$sql .= " GROUP BY a.id_data_sementara";
// echo $sql;

$query = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);


  $result = mysqli_query($conn,$sql);




  $halaman = 10;
  $page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
  $start = ($page>1) ? ($page * $halaman) - $halaman : 0;


  @$total = mysqli_num_rows($result);
  @$pages = ceil($total/@$halaman);            
  $query = mysqli_query($conn,$sql." LIMIT $start, $halaman")or die(mysqli_error($conn));
  $no =$mulai+1;




 ?>

<div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Monitoring Data Perawatan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="row">
  <div class="col-lg-12">
  <form action="" method="get">
  <input type="hidden" name="p" value="<?php echo $_GET['p'] ?>">
  <input type="hidden" name="data" value="<?php echo $_GET['data'] ?>">
  <div class="form-inline">
  <div class="form-group">
    <label>Mulai</label><br>
    <input type="date" name="mulai" class="form-control" value="<?php echo !empty($mulai) ? $mulai:'' ?>" required>
  </div>
  <div class="form-group">
    <label>Sampai</label><br>
    
    <input type="date" name="sampai" class="form-control" required="" value="<?php echo !empty($sampai) ? $sampai:'' ?>">
 
  </div>

<div class="form-group">
<br>
    <button type="submit" class="btn btn-primary">Filter</button>
</div>

  </div>
  </form>

  <table class="table table-striped">
    <thead>
      <th>#</th>
      <th>NIP</th>
      <th>Nama YMK</th>
      <th>Berita Acara Investigasi</th>
      <th>Surat Jaminan RS</th>
      <th>Surat Jaminan Jasa Raharja</th>
      <th>Surat Egibilitas BPJS</th>
      <th>Surat Kepastian Jaminan</th>
      <th>Surat Pernyataan Tidak Menjamin</th>
      <th>Keterangan</th>
    </thead>
    <tbody>
      <?php if ($num >0): 
      $no=1;
      ?>
        <?php while($data=mysqli_fetch_object($query)){
          ?>
          <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $data->NIP ?></td>
            <td><?php echo $data->nama_ymk ?></td>
            <td><?php echo $data->berita_acara != null  ? '<a class="btn btn-info" target="_blank" href="'.$base.'uploads/'.$data->berita_acara .'">PDF</a>' :'' ?></td>
            <td><?php echo $data->surat_jaminan_rs != null  ? '<a class="btn btn-info" target="_blank" href="'.$base.'uploads/'.$data->surat_jaminan_rs .'">PDF</a>' :'' ?></td>
            <td><?php echo $data->surat_jaminan_jr != null  ? '<a class="btn btn-info" target="_blank" href="'.$base.'uploads/'.$data->surat_jaminan_jr .'">PDF</a>' :'' ?></td>
            <td><?php echo $data->surat_bpjs != null  ? '<a class="btn btn-info" target="_blank" href="'.$base.'uploads/'.$data->surat_bpjs .'">PDF</a>' :'' ?></td>
            <td><?php echo $data->surat_kepastian_jaminan != null  ? '<a class="btn btn-info" target="_blank" href="'.$base.'uploads/'.$data->surat_kepastian_jaminan .'">PDF</a>' :'' ?></td>
            <td><?php echo $data->surat_pernyataan_tidak_menjamin != null  ? '<a class="btn btn-info" target="_blank" href="'.$base.'uploads/'.$data->surat_pernyataan_tidak_menjamin .'">PDF</a>' :'' ?></td>
            <td><?php echo $data->keterangan ?></td>
          </tr>
          <?php
          } ?>

      <?php else: ?>
        <tr>
        <td colspan="8"><center>Data Tidak Ditemukan</center></td>
        </tr>
      <?php endif ?>
    </tbody>
  </table>



  </div>
</div>
            </div>

              <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">


<?php              for ($i=1; $i<=$pages ; $i++){ ?>
<li class="<?php echo @$_GET['halaman']==$i ? 'active':'' ?>"> <a href="?p=monitoring&data=perawatan&halaman=<?php echo $i; ?>"><?php echo $i; ?></a></li>

 <?php } 
 ?>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

